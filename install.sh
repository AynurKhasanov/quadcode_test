#!/bin/bash

cp app/.env.example app/.env

docker-compose up -d

docker-compose run --rm php chmod -R 777 storage/logs
docker-compose run --rm php chmod -R 777 storage/framework

docker-compose run --rm php php artisan migrate:fresh --seed

docker-compose run --rm php php artisan key:generate