@extends('layouts.app')

@section('content')

    <form action="{{ route('manager.post.update',$post->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="mb-3">
            <label for="postCategorySelector" class="form-label">Category</label>
            <select aria-label="category" name="id_category" class="form-control custom-select" id="postCategorySelector">
                <option value="">Select category</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" @if($category->id == $post->id_category) selected @endif>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="postName" class="form-label">Post header</label>
            <input name="name" type="text" class="form-control" id="postName" placeholder="Post header" value="{{ $post->name }}">
        </div>

        <div class="mb-3">
            <label for="postText" class="form-label">Post text</label>
            <textarea name="text" class="form-control" id="postText" rows="3">{{ $post->text }}</textarea>
        </div>

        <div class="mb-3">
            <button type="submit" class="btn btn-primary mb-3">Save</button>
        </div>

    </form>

@endsection