@extends('layouts.app')

@section('content')

    <a class="btn btn-primary mb-3" href="{{ route('manager.post.create') }}">Create New Post</a>

    @foreach ($posts as $post)

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $post->name }}
                    <span class="badge bg-secondary">
                        {{ $post->category->name }}
                    </span>
                </h5>
                <p class="card-text">{{ $post->text }}</p>

                <form action="{{ route('manager.post.destroy',$post->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('manager.post.show',$post->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('manager.post.edit',$post->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>

    @endforeach

    {{$posts->links('vendor.pagination.bootstrap-4')}}

@endsection