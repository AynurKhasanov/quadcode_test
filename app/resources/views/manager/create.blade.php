@extends('layouts.app')

@section('content')

    <form action="{{ route('manager.post.store') }}" method="POST">
        @csrf

        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Category</label>
            <select aria-label="category" name="id_category" class="form-control custom-select">
                <option value="">Select category</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Post header</label>
            <input name="name" type="text" class="form-control" id="exampleFormControlInput1" placeholder="post name">
        </div>

        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Post text</label>
            <textarea name="text" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>

        <div class="mb-3">
            <button type="submit" class="btn btn-primary mb-3">Save</button>
        </div>

    </form>

@endsection